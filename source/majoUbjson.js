export {
    encodeWithNiceDefaults as encode,
    decodeWithNiceDefaults as decode,
};
import { encode, decode } from "../node_modules/@shelacek/ubjson/dist/ubjson.es.js";
import { createInputDecorator } from "../node_modules/fp-sac/decorators.js";
import { replaceArrayBuffers } from "./replaceArrayBuffers.js";


const replaceArrayBuffersDecorator = createInputDecorator(replaceArrayBuffers);

const encodeWithNiceDefaults = replaceArrayBuffersDecorator((object) => {
    return encode(object, {
        optimizeArrays: `onlyTypedArrays`,
    });
});


const decodeWithNiceDefaults = (uint8Array) => {
    return decode(uint8Array, {
        useTypedArrays: true,
        int64Handling: `raw`,
        highPrecisionNumberHandling: `raw`
    });
};
