export { replaceArrayBuffers };

const replaceArrayBuffers = (...inputs) => {
    return inputs.map(replaceArrayBuffersSingle);
};

const replaceArrayBuffersSingle = x => {
    if (typeof x !== `object` || !x) {
        return x;
    }
    if (x instanceof ArrayBuffer) {
        return new Uint8Array(x);
    }
    if (Array.isArray(x)) {
        return x.map(replaceArrayBuffersSingle);
    }
    if (ArrayBuffer.isView(x)) {
        return x;
    }
    const object = {};
    Object.entries(x).forEach(([key, value]) => {
        object[key] = replaceArrayBuffersSingle(value);
    });
    return object;
};
