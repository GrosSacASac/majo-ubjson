const BUILT = `built`;
const name = `majoUbjson`;


const commonOutputOptions = {
    name,
    // banner: ``,
    // footer: ``,
    // intro: ``,
    // outro: ``,
    interop: false,
    extend: false,
    strict: true,
    namespaceToStringTag: false,
};

export default {
    input: `source/majoUbjson.js`,
    treeshake: {
        propertyReadSideEffects: false // assume reading properties has no side effect
    },
    output: [
        Object.assign({
            format: `umd`,
            file: `${BUILT}/majoUbjson.umd.js`,
        }, commonOutputOptions),
        Object.assign({
            format: `es`,
            file: `${BUILT}/majoUbjson.es.js`,
        }, commonOutputOptions),
    ],

    watch: {
        clearScreen: true
    },
};
