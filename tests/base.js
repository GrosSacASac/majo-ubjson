import { encode, decode } from "../built/majoUbjson.es.js";
// import ubjson from "@shelacek/ubjson/dist/ubjson.js";
// const { encode, decode } = ubjson; // workaround

const { log } = console;

let result;

result = decode(encode(``));
log(result);

result = decode(encode({}));
log(result);

result = decode(encode([]));
log(result);

result = encode({ a: [3, 9] });
log(result);

result = decode(encode({ a: [3, 9] }));
log(result);
