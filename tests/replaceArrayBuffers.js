import { replaceArrayBuffers } from "../source/replaceArrayBuffers.js";


const { log } = console;

let result;

result = replaceArrayBuffers(``);
log(result);

result = replaceArrayBuffers({});
log(result);

result = replaceArrayBuffers([]);
log(result);

result = replaceArrayBuffers({ a: [3, 9] });
log(result);