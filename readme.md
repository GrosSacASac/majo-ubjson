# majo-ubjson

Practical ubjson. Wrapper around @shelacek/ubjson with nice defaults.

## install

`npm i  majo-ubjson`

## import

```
browser
import { encode, decode } from "./node_modules/majo-ubjson/source/majoUbjson.js";
node 12+
import { encode, decode } from "majo-ubjson";
node 
import { encode, decode } from "majo-ubjson/built/majoUbjson.umd.js";
```

## see also

https://bitbucket.org/shelacek/ubjson/src/master/
